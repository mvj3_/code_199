来源：
[http://www.36kr.com/p/201096.html](http://www.36kr.com/p/201096.html)


在浏览器地址栏中输入一行代码：`data:text/html, <html contenteditable>` ，回车即可把浏览器变临时编辑器（需要浏览器支持 HTML5 属性 contenteditable）。

经过程序员们不断改造，从一个简单的可编辑页面，逐步变成了包括支持 Java、Ruby、Python 等多种 编程语言高亮的代码编辑器，截至不到 1 个小时的最后更新，我已经看到了一个和 notepad.cc 网站功能相近，使用了第三方网站数据库 API 服务存储内容的 在线编辑器 了：

将下面代码完整复制，粘贴到 Chrome 或者 Firefox，Safari 浏览器地址栏中（不支持低版本 IE 浏览器），回车打开，稍等片刻一个支持 CTRL + S 保存内容的在线编辑器呈现眼前。